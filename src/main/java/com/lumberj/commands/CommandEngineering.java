package com.lumberj.commands;

import com.lumberj.hashs.isSelecting;
import com.mcm.core.Main;
import com.mcm.core.database.TagDb;
import com.mcm.core.database.WoodworkDb;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class CommandEngineering implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        //lenharia criar (nome)
        String[] perm = {"Superior"};
        if (command.getName().equalsIgnoreCase("woodwork") || command.getName().equalsIgnoreCase("lenharia")) {
            if (Arrays.asList(perm).contains(TagDb.getTag(uuid))) {
                if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("criar") || args[0].equalsIgnoreCase("create")) {
                        if (isSelecting.get(uuid) != null) {
                            if (isSelecting.get(uuid).getPos1() != null && isSelecting.get(uuid).getPos2() != null) {
                                Location location1 = isSelecting.get(uuid).getPos1();
                                Location location2 = isSelecting.get(uuid).getPos2();

                                int minX = location1.getBlockX() < location2.getBlockX() ? location1.getBlockX() : location2.getBlockX();
                                int maxX = location1.getBlockX() > location2.getBlockX() ? location1.getBlockX() : location2.getBlockX();
                                int minY = location1.getBlockY() < location2.getBlockY() ? location1.getBlockY() : location2.getBlockY();
                                int maxY = location1.getBlockY() > location2.getBlockY() ? location1.getBlockY() : location2.getBlockY();
                                int minZ = location1.getBlockZ() < location2.getBlockZ() ? location1.getBlockZ() : location2.getBlockZ();
                                int maxZ = location1.getBlockZ() > location2.getBlockZ() ? location1.getBlockZ() : location2.getBlockZ();

                                String blocks = null;
                                for (int y = minY; y <= maxY; y++) {
                                    for (int x = minX; x <= maxX; x++) {
                                        for (int z = minZ; z <= maxZ; z++) {
                                            Location location = new Location(location1.getWorld(), x, y, z);
                                            if (location.getBlock() != null) {
                                                if (location.getBlock().getType().name().contains("LOG") || location.getBlock().getType().name().contains("LEAVE")) {
                                                    if (blocks == null)
                                                        blocks = location.getBlock().getType().name() + "," + location.getWorld().getName() + "," + x + "," + y + "," + z; else blocks += ":" + location.getBlock().getType().name() + "," + location.getWorld().getName() + "," + x + "," + y + "," + z;
                                                }
                                            }
                                        }
                                    }
                                }

                                if (blocks == null) {
                                    player.sendMessage(Main.getTradution("c$?XdgBV#z3!4mt", uuid));
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    return false;
                                }

                                WoodworkDb.create(isSelecting.get(uuid).getPos1(), isSelecting.get(uuid).getPos2(), isSelecting.get(uuid).getName(), blocks);
                                isSelecting.get(uuid).delete();

                                player.sendMessage(Main.getTradution("mkv6KZaBrZ4qZ#Q", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                            } else {
                                player.sendMessage(Main.getTradution("7rFUBfNt8c#&HCP", uuid));
                                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else {
                            new isSelecting(uuid, args[1]).insert();

                            player.sendMessage(Main.getTradution("K!hHKF*MeA@3P?Q", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_TRADE, 1.0f, 1.0f);
                        }
                    }
                }
            }
        }
        return false;
    }
}
