package com.lumberj.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerSuffocateEvent implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void isSuffocating(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player && event.getCause().equals(EntityDamageEvent.DamageCause.SUFFOCATION)) {
            Player player = (Player) event.getEntity();
            player.teleport(new Location(player.getWorld(), player.getLocation().getX(), player.getWorld().getHighestBlockYAt(player.getLocation()), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch()));
        }
    }
}
