package com.lumberj;

import com.lumberj.commands.CommandEngineering;

public class RegisterCommands  {

    public static void register() {
        Main.instance.getCommand("woodwork").setExecutor(new CommandEngineering());
        Main.instance.getCommand("lenharia").setExecutor(new CommandEngineering());
    }
}
