package com.lumberj;

import com.mcm.core.api.ThreadAPI;
import com.mcm.core.database.WoodworkDb;
import org.bukkit.block.Block;

import java.util.concurrent.TimeUnit;

public class WoodworkTask {

    public static void run() {
        ThreadAPI.createThread(() -> {
            if (WoodworkDb.getIds() != null) {
                for (Integer id : WoodworkDb.getIds()) {
                    for (Block block : WoodworkDb.getBlocks(id)) {
                        block.getLocation().getBlock().setType(block.getType());
                    }
                }
            }
        }, TimeUnit.MINUTES.toMillis(5));
    }
}
