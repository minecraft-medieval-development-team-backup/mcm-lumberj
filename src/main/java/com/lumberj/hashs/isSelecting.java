package com.lumberj.hashs;

import org.bukkit.Location;

import java.util.HashMap;

public class isSelecting {

    private String uuid;
    private String name;
    private Location pos1;
    private Location pos2;
    private static HashMap<String, isSelecting> cache = new HashMap<String, isSelecting>();

    public isSelecting(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public isSelecting insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static isSelecting get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }

    public Location getPos1() {
        return pos1;
    }

    public void setPos1(Location pos1) {
        this.pos1 = pos1;
    }

    public Location getPos2() {
        return pos2;
    }

    public void setPos2(Location pos2) {
        this.pos2 = pos2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
