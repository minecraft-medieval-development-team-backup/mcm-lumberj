package com.lumberj;

import com.lumberj.listeners.PlayerSuffocateEvent;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new PlayerSuffocateEvent(), Main.plugin);
    }
}
